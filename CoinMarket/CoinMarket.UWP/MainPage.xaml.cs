﻿using Windows.UI.ViewManagement;

namespace CoinMarket.UWP
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            this.InitializeComponent();

            LoadApplication(new CoinMarket.App());

            ApplicationView.GetForCurrentView().TitleBar.ForegroundColor = Windows.UI.Colors.White;

            if (Helpers.Settings.DarkThemeSetting)
            {
                ApplicationView.GetForCurrentView().TitleBar.BackgroundColor = Windows.UI.Color.FromArgb(255, 33, 33, 33);
                ApplicationView.GetForCurrentView().TitleBar.ButtonInactiveBackgroundColor = Windows.UI.Color.FromArgb(255, 33, 33, 33);
                ApplicationView.GetForCurrentView().TitleBar.ButtonBackgroundColor = Windows.UI.Color.FromArgb(255, 33, 33, 33);
                ApplicationView.GetForCurrentView().TitleBar.InactiveBackgroundColor = Windows.UI.Color.FromArgb(255, 33, 33, 33);
                ApplicationView.GetForCurrentView().TitleBar.InactiveForegroundColor = Windows.UI.Color.FromArgb(255, 33, 33, 33);
            }
            else
            {
                ApplicationView.GetForCurrentView().TitleBar.BackgroundColor = Windows.UI.Color.FromArgb(255, 83, 182, 76);
                ApplicationView.GetForCurrentView().TitleBar.ButtonInactiveBackgroundColor = Windows.UI.Color.FromArgb(255, 83, 182, 76);
                ApplicationView.GetForCurrentView().TitleBar.ButtonBackgroundColor = Windows.UI.Color.FromArgb(255, 83, 182, 76);
                ApplicationView.GetForCurrentView().TitleBar.InactiveBackgroundColor = Windows.UI.Color.FromArgb(255, 83, 182, 76);
                ApplicationView.GetForCurrentView().TitleBar.InactiveForegroundColor = Windows.UI.Color.FromArgb(255, 83, 182, 76);
            }

            ApplicationView.GetForCurrentView().TitleBar.ButtonInactiveForegroundColor = Windows.UI.Colors.White;
            ApplicationView.GetForCurrentView().TitleBar.ButtonForegroundColor = Windows.UI.Colors.White;
        }
    }
}
