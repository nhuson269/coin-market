﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Gms.Ads;
using AuditApp.Android;

namespace CoinMarket.Droid
{
    [Activity(Label = "CoinMarket", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = false, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);
            MobileAds.Initialize(ApplicationContext, "ca-app-pub-4059499683571067~3214965054");
            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new App());
            Window.SetStatusBarColor(Helpers.Settings.DarkThemeSetting ? Android.Graphics.Color.Argb(255, 33, 33, 33) : Android.Graphics.Color.Argb(255, 83, 182, 76));
            AndroidPlaystoreAudit.Instance.Run(this);
        }
    }
}

