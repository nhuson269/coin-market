﻿using System;
using Android.App;
using Android.Runtime;
using AuditApp.Android;

namespace CoinMarket.Droid
{
    [Application]
    public class GlobalApplication : Application
    {
        public GlobalApplication(IntPtr handle, JniHandleOwnership transfer) : base(handle, transfer)
        {
        }

        public override void OnCreate()
        {
            base.OnCreate();

            AndroidPlaystoreAudit.Instance.UsesUntilPrompt = 3;
            AndroidPlaystoreAudit.Instance.TimeUntilPrompt = new TimeSpan(0, 0, 0);
            AndroidPlaystoreAudit.Instance.RemindLaterButtonText = "Remind me later";
            AndroidPlaystoreAudit.Instance.RemindLaterTimeToWait = new TimeSpan(0, 0, 15);
        }
    }
}