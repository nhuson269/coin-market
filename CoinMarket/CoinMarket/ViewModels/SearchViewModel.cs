﻿using CoinMarket.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace CoinMarket.ViewModels
{
    public class SearchViewModel : BaseViewModel
    {
        List<CoinModel> coinItems;

        public ObservableCollection<CoinModel> SearchItems { get; set; }

        public SearchViewModel(List<CoinModel> coinItems)
        {        
            Title = "Search";

            if (Device.RuntimePlatform == Device.iOS)
                AdUnitId = "ca-app-pub-4059499683571067/2727524673";
            else if (Device.RuntimePlatform == Device.Android)
                AdUnitId = "ca-app-pub-4059499683571067/5267726408";

            this.coinItems = new List<CoinModel>();
            SearchItems = new ObservableCollection<CoinModel>();
            this.coinItems = coinItems;
        }

        public void SearchCoin(string str)
        {
            SearchItems.Clear();

            foreach (var item in coinItems)
            {
                if (item.Name.ToLower().Contains(str.ToLower()) || (item.Symbol.ToLower().Contains(str.ToLower())))
                {
                    SearchItems.Add(item);
                }
                else
                {
                    continue;
                }
            }
        }
    }
}
