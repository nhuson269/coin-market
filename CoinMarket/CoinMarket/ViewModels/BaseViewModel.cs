﻿using CoinMarket.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace CoinMarket.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        public ICoinClient CoinClient = DependencyService.Get<ICoinClient>() ?? new CoinClient();

        bool isLoad = false;
        public bool IsLoad
        {
            get { return isLoad; }
            set { SetProperty(ref isLoad, value); }
        }

        string title = string.Empty;
        public string Title
        {
            get { return title; }
            set { SetProperty(ref title, value); }
        }

        string last_Update = "Loading...";
        public string LastUpdate
        {
            get { return last_Update; }
            set { SetProperty(ref last_Update, value); }
        }

        string ad_unitId = string.Empty;
        public string AdUnitId
        {
            get { return ad_unitId; }
            set { SetProperty(ref ad_unitId, value); }
        }

        protected bool SetProperty<T>(ref T backingStore, T value,
           [CallerMemberName]string propertyName = "",
           Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

#region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;

            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
#endregion
    }
}
