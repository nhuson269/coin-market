﻿using System;
using CoinMarket.Models;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Collections.Generic;

namespace CoinMarket.ViewModels
{
    public class HomePageViewModel : BaseViewModel
    {
        public List<CoinModel> allCoinItems;
        public ObservableCollection<CoinModel> CoinItems { get; set; }
        public Command LoadCoinItemsCommand { get; set; }

        public HomePageViewModel()
        {
            Title = "CoinMarket";

            if (Device.RuntimePlatform == Device.iOS)
                AdUnitId = "ca-app-pub-4059499683571067/2727524673";
            else if (Device.RuntimePlatform == Device.Android)
                AdUnitId = "ca-app-pub-4059499683571067/5267726408";

            allCoinItems = new List<CoinModel>();
            CoinItems = new ObservableCollection<CoinModel>();
            LoadCoinItemsCommand = new Command(async () => await LoadCoinItems());
        }

        async Task LoadCoinItems()
        {
            if (IsLoad) return;

            IsLoad = true;
            var saveTimeLastUpdate = LastUpdate;
            LastUpdate = "Loading...";
            var gCoinItems = await CoinClient.GetCoins();

            if (gCoinItems.Count <= 0)
            {
                LastUpdate = allCoinItems.Count <= 0 ? "No content was found!" : saveTimeLastUpdate;
            }
            else
            {
                LastUpdate = "Last updated on " + DateTime.Now.ToString();
                allCoinItems = gCoinItems;
                CoinItems.Clear();
                byte i = 0;

                foreach (var item in allCoinItems)
                {
                    if (i < 100)
                    {
                        CoinItems.Add(item);
                        i++;
                    }
                    else
                    {
                        break;
                    }
                }
            }

            IsLoad = false;
        }
    }
}
