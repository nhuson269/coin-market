﻿using CoinMarket.Models;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CoinMarket.ViewModels
{
    public class CoinDetailViewModel : BaseViewModel
    {
        public ObservableCollection<CoinModel> CoinItem { get; set; }
        public Command LoadCoinItemCommand { get; set; }

        string convert_value = string.Empty;
        public string ConvertValue
        {
            get { return convert_value; }
            set { SetProperty(ref convert_value, value); }
        }

        string coinId;
        public CoinDetailViewModel(CoinModel coin)
        {
            Title = coin.Name + " (" + coin.Symbol + ")";

            if (Device.RuntimePlatform == Device.iOS)
                AdUnitId = "ca-app-pub-4059499683571067/2727524673";
            else if (Device.RuntimePlatform == Device.Android)
                AdUnitId = "ca-app-pub-4059499683571067/5267726408";

            coinId = coin.Id;
            ConvertValue = Helpers.Settings.CurrencyCode;
            CoinItem = new ObservableCollection<CoinModel>();
            LoadCoinItemCommand = new Command(async () => await LoadCoinItem());
        }

        public void ConvertCoin(string str)
        {
            if (!string.IsNullOrEmpty(str))
            {
                string vl = (double.Parse(str) * double.Parse(CoinItem[0].PriceConvert.Replace(".", ","))).ToString();
                ConvertValue = Helpers.Settings.FormatDecimal(vl.Replace(",",".")) + " " + Helpers.Settings.CurrencyCode;
            }
            else
            {
                ConvertValue = Helpers.Settings.CurrencyCode;
            }
        }

        async Task LoadCoinItem()
        {
            if (IsLoad) return;

            IsLoad = true;
            var saveTimeLastUpdate = LastUpdate;
            LastUpdate = "Loading...";
            var item = await CoinClient.GetCoin(coinId);
           
            if (item.Count <= 0)
            {
                LastUpdate = CoinItem.Count <= 0 ? "No content was found!" : saveTimeLastUpdate;
            }
            else
            {
                LastUpdate = "Last updated on " + DateTime.Now.ToString();
                CoinItem.Clear();
                foreach (var oitem in item)
                {
                    CoinItem.Add(oitem);
                }
            }

            IsLoad = false;
        }
    }
}
