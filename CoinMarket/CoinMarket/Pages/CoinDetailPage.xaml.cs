﻿using CoinMarket.ViewModels;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CoinMarket.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CoinDetailPage : ContentPage
	{
        CoinDetailViewModel viewModel;

        public CoinDetailPage(CoinDetailViewModel viewModel)
        {
            InitializeComponent();

            if (Device.RuntimePlatform == Device.UWP)
            {
                ToolbarItem rfToolbarItem = new ToolbarItem()
                {
                    Text = "Refresh",
                    Icon = "iconrefresh.png"
                };
                rfToolbarItem.Clicked += RfToolbarItem_Clicked;
                ToolbarItems.Add(rfToolbarItem);
            }

            this.viewModel = viewModel;
            BindingContext = viewModel;

            Device.StartTimer(TimeSpan.FromMinutes(5), () =>
            {
                if (Helpers.Settings.AutoUpdateRateSetting)
                {
                    viewModel.LoadCoinItemCommand.Execute(null);
                }
                return true;
            });
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.CoinItem.Count == 0)
                viewModel.LoadCoinItemCommand.Execute(null);
        }

        private void RfToolbarItem_Clicked(object sender, EventArgs e)
        {
            viewModel.LoadCoinItemCommand.Execute(null);
        }

        void OnCoinItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            if (args == null) return;

            CoinItemListView.SelectedItem = null;
        }

        void Convert_Clicked(object sender, EventArgs e)
        {
            slConvert.IsVisible = !slConvert.IsVisible;
        }

        void entry_TextChanged(object sender, TextChangedEventArgs e)
        {
            viewModel.ConvertCoin(e.NewTextValue);
        }
    }
}