﻿using CoinMarket.Models;
using CoinMarket.ViewModels;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CoinMarket.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HomePage : ContentPage
	{
        HomePageViewModel viewModel;
        public HomePage()
        {
            InitializeComponent();

            if (Device.RuntimePlatform == Device.UWP)
            {
                ToolbarItem rfToolbarItem = new ToolbarItem()
                {
                    Text = "Refresh",
                    Icon = "iconrefresh.png"
                };
                rfToolbarItem.Clicked += RfToolbarItem_Clicked;
                ToolbarItems.Add(rfToolbarItem);
            }

            viewModel = new HomePageViewModel();
            BindingContext = viewModel;

            Device.StartTimer(TimeSpan.FromMinutes(5), () =>
            {
                if (Helpers.Settings.AutoUpdateRateSetting)
                {
                    viewModel.LoadCoinItemsCommand.Execute(null);
                }
                return true;
            });
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.CoinItems.Count == 0)
                viewModel.LoadCoinItemsCommand.Execute(null);
        }

        private void RfToolbarItem_Clicked(object sender, EventArgs e)
        {
            viewModel.LoadCoinItemsCommand.Execute(null);
        }

        async void Search_Clicked(object sender, EventArgs e)
        {
            if (viewModel.CoinItems.Count > 0)
            {
                await Navigation.PushAsync(new SearchPage(new SearchViewModel(viewModel.allCoinItems)));
            }
            else
            {
                await DisplayAlert("CoinMarket", "Can not search now", "OK");
            }
        }

        async void Setting_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AboutPage());
        }

        async void OnCoinItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var coinItem = args.SelectedItem as CoinModel;
            if (coinItem == null)
                return;

            CoinItemsListView.SelectedItem = null;
            await Navigation.PushAsync(new CoinDetailPage(new CoinDetailViewModel(coinItem)));
        }
    }
}