﻿using CoinMarket.Models;
using CoinMarket.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CoinMarket.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SearchPage : ContentPage
	{
        SearchViewModel viewModel;
		public SearchPage(SearchViewModel viewModel)
		{
			InitializeComponent();
            this.viewModel = viewModel;
            BindingContext = viewModel;
		}

        void entry_TextChanged(object sender, TextChangedEventArgs e)
        {
            viewModel.SearchCoin(e.NewTextValue);
        }

        async void OnCoinItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var coinItem = args.SelectedItem as CoinModel;
            if (coinItem == null)
                return;

            SearchItemsListView.SelectedItem = null;
            await Navigation.PushAsync(new CoinDetailPage(new CoinDetailViewModel(coinItem)));
        }
    }
}