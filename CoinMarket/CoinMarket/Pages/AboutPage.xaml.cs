﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CoinMarket.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AboutPage : ContentPage
	{
		public AboutPage ()
		{
			InitializeComponent ();
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();

            switchDarkTheme.IsToggled = Helpers.Settings.DarkThemeSetting;
            switchAutoUpdate.IsToggled = Helpers.Settings.AutoUpdateRateSetting;
            lbLocalCurrency.Text = Helpers.Settings.CurrencyCode;
        }

        async void switchDarkTheme_Toggled(object sender, ToggledEventArgs e)
        {
            if (Helpers.Settings.DarkThemeSetting != e.Value)
            {
                await DisplayAlert("CoinMarket", "Please, open the application again to see the changes.", "OK");
            }

            Helpers.Settings.DarkThemeSetting = e.Value;
            Helpers.Settings.DarkThemeSettings = e.Value.ToString();
        }

        void switchAutoUpdate_Toggled(object sender, ToggledEventArgs e)
        {
            Helpers.Settings.AutoUpdateRateSetting = e.Value;
            Helpers.Settings.AutoUpdateSettings = e.Value.ToString();
        }

        async void LocalCurrency_Clicked(object sender, EventArgs e)
        {          
            var action = await DisplayActionSheet("Local Currency", "Cancel", null, "USD", "AUD", "BRL", "CAD", "CHF", "CLP", "CNY", "CZK", "DKK", "EUR", "GBP", "HKD", "HUF", "IDR", "ILS", "INR", "JPY", "KRW", "MXN", "MYR", "NOK", "NZD", "PHP", "PKR", "PLN", "RUB", "SEK", "SGD", "THB", "TRY", "TWD", "ZAR");
            lbLocalCurrency.Text = action.Equals("Cancel") ? Helpers.Settings.CurrencyCode : action;
            Helpers.Settings.CurrencyCode = lbLocalCurrency.Text;
            Helpers.Settings.CurrencyCodeSettings = lbLocalCurrency.Text;
        }

        void Rate_Clicked(object sender, EventArgs e)
        {
            var url = string.Empty;
            var appId = string.Empty;

            if (Device.RuntimePlatform == Device.iOS)
            {
                appId = "com.ninesoft.CoinMarket";
                url = $"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id={appId}&amp;onlyLatestVersion=true&amp;pageNumber=0&amp;sortOrdering=1&amp;type=Purple+Software";
            }
            else if (Device.RuntimePlatform == Device.Android)
            {
                appId = "com.ninesoft.CoinMarket";
                url = $"https://play.google.com/store/apps/details?id={appId}";
            }
            else if (Device.RuntimePlatform == Device.UWP)
            {
                appId = "9NFW6FSFQH95";
                url = $"ms-windows-store://pdp/?productid={appId}";
            }

            Device.OpenUri(new Uri(url));
        }
    }
}