﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace CoinMarket.Converters
{
    public class PriceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((value is string str) && (!string.IsNullOrEmpty(str)))
            {
                return Helpers.Settings.CurrencySymbol[Helpers.Settings.CurrencyCode] + Helpers.Settings.FormatDecimal(str);
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
