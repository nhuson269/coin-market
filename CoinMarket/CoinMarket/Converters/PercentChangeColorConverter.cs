﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace CoinMarket.Converters
{
    public class PercentChangeColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((value is string str) && (!string.IsNullOrEmpty(str)))
            {
                return !str.StartsWith("-") ? Color.FromHex("#009933") : Color.FromHex("#d14836");
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
