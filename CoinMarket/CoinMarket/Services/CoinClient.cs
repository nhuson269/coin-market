﻿using CoinMarket.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoinMarket.Services
{
    public class CoinClient : ICoinClient
    {
        HttpClient httpClient;
        public CoinClient()
        {
            httpClient = new HttpClient
            {
                BaseAddress = new Uri(ClientKeys.BASE_URL)
            };
        }
        public async Task<List<CoinModel>> GetCoins(int start = 0, int limit = 0)
        {
            try
            {
                HttpResponseMessage response = await httpClient.GetAsync("ticker/" + ClientKeys.GetCurrencyConvertPath("start=" + start + "&limit=" + limit));

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    List<CoinModel> coinItems = new List<CoinModel>();
                    var coinDictionary = CoinModelJson.FromJson(await response.Content.ReadAsStringAsync());
                    for (int i = 0; i < coinDictionary.Length; i++)
                    {
                        try
                        {
                            coinItems.Add(new CoinModel
                            {
                                Id = coinDictionary[i]["id"],
                                Name = coinDictionary[i]["name"],
                                Symbol = coinDictionary[i]["symbol"],
                                Rank = "No." + coinDictionary[i]["rank"],
                                PriceConvert = Helpers.Settings.CurrencySymbol[Helpers.Settings.CurrencyCode] + Helpers.Settings.FormatDecimal(Helpers.Settings.CurrencyCode.Equals("USD") ? coinDictionary[i]["price_usd"] ?? "?" : coinDictionary[i]["price_" + Helpers.Settings.CurrencyCode.ToLower()] ?? "?"),
                                PercentChange1h = coinDictionary[i]["percent_change_1h"] ?? "?",
                                PercentChange24h = coinDictionary[i]["percent_change_24h"] ?? "?"
                            });
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine("ERROR-CreateCoinModel: " + ex.Message);
                            continue;
                        }
                    }

                    return await Task.FromResult(coinItems);
                }

                Debug.WriteLine("ERROR-GetCoins-ResponseStatusCode=" + response.StatusCode.ToString());
                return await Task.FromResult(new List<CoinModel>());
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ERROR-GetCoins: " + ex.Message);
                return await Task.FromResult(new List<CoinModel>());
            }

        }

        public async Task<List<CoinModel>> GetCoin(string id)
        {
            try
            {
                HttpResponseMessage response = await httpClient.GetAsync("ticker/" + id + "/" + ClientKeys.GetCurrencyConvertPath(""));

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    try
                    {
                        var coinDictionary = CoinModelJson.FromJson(await response.Content.ReadAsStringAsync());
                        List<CoinModel> coinItem = new List<CoinModel>();

                        coinItem.Add(new CoinModel
                        {
                            Id = coinDictionary[0]["id"],
                            Name = coinDictionary[0]["name"],
                            Symbol = coinDictionary[0]["symbol"],
                            Rank = "No." + coinDictionary[0]["rank"],
                            PriceConvert = Helpers.Settings.CurrencyCode.Equals("USD") ? coinDictionary[0]["price_usd"] ?? "?" : coinDictionary[0]["price_" + Helpers.Settings.CurrencyCode.ToLower()] ?? "?",
                            PriceBtc = coinDictionary[0]["price_btc"] ?? "?",
                            Volume24hConvert = Helpers.Settings.CurrencyCode.Equals("USD") ? coinDictionary[0]["24h_volume_usd"] ?? "?" : coinDictionary[0]["24h_volume_" + Helpers.Settings.CurrencyCode.ToLower()] ?? "?",
                            MarketCapConvert = Helpers.Settings.CurrencyCode.Equals("USD") ? coinDictionary[0]["market_cap_usd"] ?? "?" : coinDictionary[0]["market_cap_" + Helpers.Settings.CurrencyCode.ToLower()] ?? "?",
                            TotalSupply = coinDictionary[0]["total_supply"] ?? "?",
                            MaxSupply = coinDictionary[0]["max_supply"] ?? "?",
                            PercentChange1h = coinDictionary[0]["percent_change_1h"] ?? "?",
                            PercentChange24h = coinDictionary[0]["percent_change_24h"] ?? "?",
                            PercentChange7d = coinDictionary[0]["percent_change_7d"] ?? "?",
                            CurrencyCode = Helpers.Settings.CurrencyCode
                        });

                        return await Task.FromResult(coinItem);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("ERROR-CreateCoinModel: " + ex.Message);
                        return await Task.FromResult(new List<CoinModel>());
                    }
                }

                Debug.WriteLine("ERROR-GetCoin-ResponseStatusCode=" + response.StatusCode.ToString());
                return await Task.FromResult(new List<CoinModel>());
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ERROR-GetCoin: " + ex.Message);
                return await Task.FromResult(new List<CoinModel>());
            }
        }
    }
}