﻿namespace CoinMarket.Services
{
    public class ClientKeys
    {
        public const string BASE_URL = "https://api.coinmarketcap.com/v1/";

        public static string GetCurrencyConvertPath(string str)
        {
            if (!Helpers.Settings.CurrencyCode.Equals("USD"))
            {
                if (!string.IsNullOrEmpty(str))
                {
                    return "?convert=" + Helpers.Settings.CurrencyCode + "&" + str;
                }
                else
                {
                    return "?convert=" + Helpers.Settings.CurrencyCode;
                }
            }

            return !string.IsNullOrEmpty(str) ? "?" + str : "";
        }
    }
}