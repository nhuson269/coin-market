﻿using CoinMarket.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoinMarket.Services
{
    public interface ICoinClient
    {
        Task<List<CoinModel>> GetCoins(int start = 0, int limit = 0);
        Task<List<CoinModel>> GetCoin(string id);
    }
}
