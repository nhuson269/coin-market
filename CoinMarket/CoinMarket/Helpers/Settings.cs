﻿using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System;
using System.Collections.Generic;

namespace CoinMarket.Helpers
{
    /// <summary>
    /// This is the Settings static class that can be used in your Core solution or in any
    /// of your client applications. All settings are laid out the same exact way with getters
    /// and setters. 
    /// </summary>
    public static class Settings
    {
        public static bool DarkThemeSetting = false;
        public static bool AutoUpdateRateSetting = false;
        public static string CurrencyCode = "USD";
       
        public static Dictionary<string, string> CurrencySymbol = new Dictionary<string, string>
        {
            { "USD", "$" }, { "AUD", "A$" }, { "BRL", "R$" }, { "CAD", "$" }, { "CHF", "Fr. " },
            { "CLP", "$" },{ "CNY", "¥" }, { "CZK", "Kč" }, { "DKK", "kr. " }, { "EUR", "€" },
            { "GBP", "£" }, { "HKD", "$" }, { "HUF", "Ft " }, { "IDR", "Rp " }, { "ILS", "₪" },
            { "INR", "₹" }, { "JPY", "¥" }, { "KRW", "₩" }, { "MXN", "$" }, { "MYR", "RM" },
            { "NOK", "kr" }, { "NZD", "$" }, { "PHP", "₱" }, { "PKR", "Rs " }, { "PLN", "zł" },
            { "RUB", "₽" }, { "SEK", "kr " }, { "SGD", "S$" }, { "THB", "฿" }, { "TRY", "₺" },
            { "TWD", "NT$" }, { "ZAR", "R " }
        };

        public static string FormatDecimal(string s)
        {
            try
            {
                string[] str = s.Split('.');
                string stmp = string.Format("{0:n0}", Int64.Parse(str[0])).Replace(".",",");
                return str.Length > 1 ? stmp + "." + str[1] : stmp;
            }
            catch
            {
                return s;
            }
        }

        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        #region Setting Dark Theme
        private const string DarkThemeSettingsKey = "dark_theme";
        private static readonly string DarkThemeSettingsDefault = "false";

        public static string DarkThemeSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault(DarkThemeSettingsKey, DarkThemeSettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(DarkThemeSettingsKey, value);
            }
        }
        #endregion

        #region Setting Automatically Update Rates
        private const string AutoUpdateSettingsKey = "auto_update";
        private static readonly string AutoUpdateSettingsDefault = "false";

        public static string AutoUpdateSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault(AutoUpdateSettingsKey, AutoUpdateSettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(AutoUpdateSettingsKey, value);
            }
        }
        #endregion

        #region Setting Currency Code
        private const string CurrencyCodeSettingsKey = "currency_code";
        private static readonly string CurrencyCodeSettingsDefault = "USD";

        public static string CurrencyCodeSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault(CurrencyCodeSettingsKey, CurrencyCodeSettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(CurrencyCodeSettingsKey, value);
            }
        }
        #endregion
    }
}