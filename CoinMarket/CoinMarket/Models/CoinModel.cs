﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using System.Globalization;

namespace CoinMarket.Models
{
    public class CoinModel
    {
        public string Id { get; set; } //"id": "bitcoin"
        public string Name { get; set; } //"name": "Bitcoin"
        public string Symbol { get; set; } //"symbol": "BTC"
        public string Rank { get; set; } //"rank": "1"
        public string PriceUSD { get; set; }  //"price_usd": "9898.56"
        public string PriceBtc { get; set; } //"price_btc": "1.0"
        public string Volume24Usd { get; set; } //"24h_volume_usd": "9252160000.0"
        public string MarketCapUsd { get; set; } //"market_cap_usd": "167358289735"
        public string AvailableSupply { get; set; } //"available_supply": "16907337.0"
        public string TotalSupply { get; set; } //"total_supply": "16907337.0"
        public string MaxSupply { get; set; } //"max_supply": "21000000.0"
        public string PercentChange1h { get; set; } //"percent_change_1h": "0.73"
        public string PercentChange24h { get; set; } //"percent_change_24h": "-7.31"
        public string PercentChange7d { get; set; } //"percent_change_7d": "-8.12"
        public string LastUpdated { get; set; } //"last_updated": "1520519365"
        public string PriceConvert { get; set; } //"price_eur": "7996.06666656"
        public string Volume24hConvert { get; set; } //"24h_volume_eur": "7473904100.16"
        public string MarketCapConvert { get; set; } //"market_cap_eur": "135192193806"
        public string CurrencyCode { get; set; } // Setting save Currency Code
    }

    public class CoinModelJson
    {
        public static Dictionary<string, string>[] FromJson(string json) => JsonConvert.DeserializeObject<Dictionary<string, string>[]>(json, Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this Dictionary<string, string>[] self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    internal class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
