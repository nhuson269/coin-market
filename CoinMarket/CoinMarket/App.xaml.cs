﻿using Xamarin.Forms;

namespace CoinMarket
{
    public partial class App : Application
	{
		public App ()
		{
			InitializeComponent();

            Helpers.Settings.DarkThemeSetting = bool.Parse(Helpers.Settings.DarkThemeSettings);
            if (Helpers.Settings.DarkThemeSetting)
            {
                Current.Resources["BarBackgroundColor"] = Color.FromHex("#212121");
                Current.Resources["BackgroundBasicColor"] = Color.FromHex("#303030");
                Current.Resources["TextBasicColor"] = Color.White;
                Current.Resources["SeparatorColor"] = Color.FromHex("#494949");
            }

            Helpers.Settings.CurrencyCode = Helpers.Settings.CurrencyCodeSettings;
            MainPage = new NavigationPage(new Pages.HomePage())
            {
                BarBackgroundColor = Helpers.Settings.DarkThemeSetting ? Color.FromHex("#212121") : Color.FromHex("#53b64c"),
                BarTextColor = Color.White
            };
        }

		protected override void OnStart ()
		{
            // Handle when your app starts
            Helpers.Settings.AutoUpdateRateSetting = bool.Parse(Helpers.Settings.AutoUpdateSettings); 
        }

        protected override void OnSleep ()
		{
            // Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
